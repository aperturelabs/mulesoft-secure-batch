# Mulesoft Secure Batch

This simple batch file encrypts or decrypts a value using Mulesoft's secure properties JAR file.
This version uses Blowfish CBC, but you can easily edit it to use whatever methodology you prefer.
The output is shown in the CLI and copied to clipboard.

To use:
1. Download the Secure Properties Tool [from Mulesoft](https://docs.mulesoft.com/mule-runtime/4.2/secure-configuration-properties#secure_props_tool).
2. Rename it to "secure.jar" and place it in the **same directory** as this batch file.
4. Run the batch file and follow the prompts.
5. The result will be displayed and copied to your clipboard.
6. If desired, continue entering values to encrypt/decrypt.
7. Leave the input empty and press Enter to exit.

To discuss this project or report problems, please visit the [Reddit post](https://www.reddit.com/r/MuleSoft/comments/dq5wpt/batch_file_for_secure_properties/).

Please see the LICENSE file for licensing and disclaimer information.