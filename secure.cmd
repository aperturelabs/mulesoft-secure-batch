@echo off
cls
echo BLOWFISH ENCRYPTOR/DECRYPTOR

REM CD to the directory of this batch file

cd /D "%~dp0"

REM Set encrypt vs decrypt mode

set /p mode="e (encrypt) or d (decrypt): "

IF "%mode%"=="d" (
	SET mode=decrypt
) ELSE (
	SET mode=encrypt
)

REM Set cryptographic key

set /p key="Enter key: "

REM Enter loop to get multiple values

:DoWhile
set /p value="Enter value: "

IF NOT DEFINED value (GOTO :EndDoWhile)

java -jar secure.jar string %mode% Blowfish CBC %key% %value%
java -jar secure.jar string %mode% Blowfish CBC %key% %value% | clip

echo.
echo Result copied to clipboard. Enter another value to %mode% or press Enter to exit.
echo.

set value=

GOTO :DoWhile

:EndDoWhile